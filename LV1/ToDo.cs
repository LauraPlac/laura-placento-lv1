﻿using System;
using System.Collections.Generic;
using System.Text;


//Z7.
namespace LV1
{
    class ToDo
    {
        List<zabiljeska> zabiljeske;

        public ToDo()
        {
            zabiljeske = new List<zabiljeska>();
        }

        public void DodajZabiljesku(zabiljeska zabiljeska)
        {
            zabiljeske.Add(zabiljeska);
        }

        public void ObavljanjeZadataka()
        {
            for(int i = 0; i < zabiljeske.Count; i++)
            {
                int maxRazinaVaznosti = this.NadiMaxRazinaVaznosti();
                if(zabiljeske[i].RazinaVaznosti == maxRazinaVaznosti)
                {
                    zabiljeske.RemoveAt(i);
                    i--;
                }
            }
        }

        private int NadiMaxRazinaVaznosti()
        {
            int max = 0;
            foreach(var zabiljeska in zabiljeske)
            {
                if(zabiljeska.RazinaVaznosti > max)
                {
                    max = zabiljeska.RazinaVaznosti;
                }
            }
            return max;
        }

        public override string ToString()
        {
            StringBuilder stringBuilder = new StringBuilder();
            foreach(var zabiljeska in zabiljeske)
            {
                stringBuilder.Append(zabiljeska).Append("\n");
            }
            return stringBuilder.ToString();
        }
    }
}
