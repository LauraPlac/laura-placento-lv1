﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace LV1
{
    class zabiljeska
    {
        String biljezenjeTeksta;
        String autor;
        int razinaVaznosti;
         
        //Z4.
        public String BiljezenjeTeksta
        {
            get { return this.biljezenjeTeksta; }
            set { this.biljezenjeTeksta = value; }
        }

        //Z4.
        public String Autor
        {
            get { return this.autor; }
            private set { this.autor = value; }
        }

        //Z4.
        public int RazinaVaznosti
        {
            get { return this.razinaVaznosti; }
            set 
            { 
                if(value < 1 || value > 5)
                {
                    this.razinaVaznosti = 1;
                }
                else
                {
                    this.razinaVaznosti = value;
                }
            }
        }


        //Z2.
        public zabiljeska(String biljezenjeTeksta, String autor, int razinaVaznosti)
        {
            this.biljezenjeTeksta = biljezenjeTeksta;
            this.autor = autor;
            if (razinaVaznosti < 1 || razinaVaznosti > 5)
            {
                this.razinaVaznosti = 1;
            }
            else
            {
                this.razinaVaznosti = razinaVaznosti;
            }
        }
    
        //Z3.
        public zabiljeska()
        {
            this.biljezenjeTeksta = "Hehe";
            this.autor = String.Empty;
            this.razinaVaznosti = 1;
        }

        //Z3.
        public zabiljeska(String biljezenjeTeksta, String autor)
        {
            this.biljezenjeTeksta = biljezenjeTeksta;
            this.autor = autor;
            this.razinaVaznosti = 1;
        }

        
        public void SetBiljezenjeTeksta(String newBiljezenjeTeksta)
        {
            this.biljezenjeTeksta = newBiljezenjeTeksta;
        }

        public void SetRazinaVaznosti(int newRazinaVaznosti)
        {
            if (newRazinaVaznosti < 1 || newRazinaVaznosti > 5)
            {
                this.razinaVaznosti = 1;
            }
            else
            {
                this.razinaVaznosti = newRazinaVaznosti;
            }
        }
    

        public String GetBiljezenjeTeksta()
        {
            return this.biljezenjeTeksta;
        }

        public int GetRazinaVaznosti()
        {
            return this.razinaVaznosti;
        }

        //Z3.
        public String GetAutor()
        {
            return this.autor;
        }

        //Z5.
        public override string ToString()
        {
            return "Autor: " + this.Autor + ", Tekst: " + this.BiljezenjeTeksta + ", Razina vaznosti: " + this.RazinaVaznosti;
        }


    }
}
