﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV1
{
    class Program
    {
        static void Main(string[] args) 
        {
            //Z3.
            zabiljeska biljeska1 = new zabiljeska();
            zabiljeska biljeska2 = new zabiljeska("Tekst", "Laura");
            zabiljeska biljeska3 = new zabiljeska("Tekst2", "Laura", 5);

            Console.WriteLine($"Tekst1: {biljeska1.GetBiljezenjeTeksta()}, Autor: {biljeska1.GetAutor()}");
            Console.WriteLine($"Tekst2: {biljeska2.GetBiljezenjeTeksta()}, Autor: {biljeska2.GetAutor()}");
            Console.WriteLine($"Tekst3: {biljeska3.GetBiljezenjeTeksta()}, Autor: {biljeska3.GetAutor()}");

            //Z4.
            biljeska3.BiljezenjeTeksta = "Promijenjeno svojstvo";


            //Z5.
            Console.WriteLine(biljeska1.ToString());
            Console.WriteLine(biljeska2);
            Console.WriteLine(biljeska3);

            //Z6.
            VremenskaZabiljeska zabiljeska = new VremenskaZabiljeska("Tekst", "Laura", 5);
            Console.WriteLine(zabiljeska);

            //Z7.
            ToDo toDo1 = new ToDo();
            toDo1.DodajZabiljesku(biljeska1);
            toDo1.DodajZabiljesku(biljeska2);
            toDo1.DodajZabiljesku(biljeska3);
            toDo1.DodajZabiljesku(zabiljeska);
            Console.WriteLine(toDo1);
            toDo1.ObavljanjeZadataka();
            Console.WriteLine(toDo1);
        }
        
    }
}
