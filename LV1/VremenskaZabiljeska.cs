﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;


//Z6.
namespace LV1
{
    class VremenskaZabiljeska : zabiljeska
    {

        DateTime vremenskaOznaka;

        public DateTime VremenskaOznaka
        {
            get { return this.vremenskaOznaka; }
            set { this.vremenskaOznaka = value; }
        }

        public VremenskaZabiljeska (String biljezenjeTeksta, String autor, int razinaVaznosti) : base(biljezenjeTeksta, autor, razinaVaznosti)
        {
            vremenskaOznaka = DateTime.Now;
        }

        public override string ToString()
        {
            return $"[{vremenskaOznaka.Hour}:{vremenskaOznaka.Minute}:{vremenskaOznaka.Second}] {base.Autor}: {base.BiljezenjeTeksta} (Razina vaznosti: {base.RazinaVaznosti})";

        }


    }
}
